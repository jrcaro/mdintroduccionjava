package org.uma.mbd.mdHospitalV1L.hospital;

public class Persona {
    private String nombre, apellidos, dni;
    private int edad;
    private Genero sexo; //enum genero
    public Persona(String name, String lname, String id, int age, Genero gen){
        nombre = name;
        apellidos = lname;
        dni = id;
        edad = age;
        sexo = gen;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDni() {
        return dni;
    }

    public int getEdad() {
        return edad;
    }

    public Genero getSexo() {
        return sexo;
    }

    @Override
    public String toString() {
        return nombre + " " + apellidos + "; " +
                edad + "; " + sexo + "; " + dni;
    }
}
