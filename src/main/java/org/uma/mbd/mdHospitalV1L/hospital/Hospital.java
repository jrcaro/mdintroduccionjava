package org.uma.mbd.mdHospitalV1L.hospital;

import java.util.List;

public class Hospital {
    private int numDepartamentos;
    private String nombre, direccion;
    List<Departamento> departamentos;
    public Hospital(String name, String direcc, List<Departamento> dep, int nDep){
        nombre = name;
        direccion = direcc;
        departamentos = dep;
        numDepartamentos = nDep;
    }

    public Departamento getDepartamento(String name) {
        int i = 0;
        while(i < departamentos.size() && !name.equalsIgnoreCase(departamentos.get(i).getNombre())){
            i++;
        }
        return departamentos.get(i);
    }

    public int getNumDepartamentos() {
        return numDepartamentos;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    @Override
    public String toString() {
        return "Hospital(" + departamentos + ')';
    }
}
