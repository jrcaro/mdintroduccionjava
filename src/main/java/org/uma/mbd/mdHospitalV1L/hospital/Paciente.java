package org.uma.mbd.mdHospitalV1L.hospital;

public class Paciente extends Persona{
    private Medico medico;
    private boolean esUrgencia;
    private String numSegSocial;
    private double altura, indiceMasaCorporal, peso;
    public Paciente(String id, String name, String lname, int age, Genero gen, double alt, double p, String nSeg, boolean urg) {
        super(name, lname, id, age, gen);
        esUrgencia = urg;
        numSegSocial = nSeg;
        altura = alt;
        peso = p;
        medico = null;
        indiceMasaCorporal = peso / altura;
    }

    public boolean esUrgencia(){
        return esUrgencia;
    }

    public void asignaMedico(Medico med){
        medico = med;
    }

    public Medico atendidoPor(){
        return medico;
    }

    public String getNumSegSocial() {
        return numSegSocial;
    }

    public double getAltura() {
        return altura;
    }

    public double getIndiceMasaCorporal() {
        return indiceMasaCorporal;
    }

    public double getPeso() {
        return peso;
    }

    @Override
    public String toString() {
        return super.toString() + "; " + numSegSocial + "; " +
                altura + "; " + indiceMasaCorporal + "; " + peso +
                "; " + medico;
    }
}
