package org.uma.mbd.mdHospitalV1L.hospital;

import java.util.List;

public class Departamento {
    private List<Medico> medicos;
    private String nombre;
    public Departamento(String name, List<Medico> listMed){
        nombre = name;
        medicos = listMed;
    }

    public Medico getMedico(String dni){
        int i = 0;
        while(i < medicos.size() && !dni.equalsIgnoreCase(medicos.get(i).getDni())){
            i++;
        }

        return medicos.get(i);
    }

    public boolean trabajaEnDepartamento(Medico med){
        return medicos.contains(med); //??
    }

    public int numMedicos(Categoria cat){
        int cont = 0;
        for (int i = 0; i < medicos.size(); i++) {
            if(medicos.get(i).getCategoriaProfesional() == cat){
                cont++;
            }
        }
        return cont;
    }

    public int getNumMedicos(){
        return medicos.size();
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "Departamento(" + nombre + "; " + medicos + ")";
    }
}
