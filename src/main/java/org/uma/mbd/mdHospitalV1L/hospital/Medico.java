package org.uma.mbd.mdHospitalV1L.hospital;

public class Medico extends Persona {
    private Categoria categoriaProfesional;
    private double horasSemanales;
    private boolean trabajaEnPrivado;
    public Medico(String id, String name, String lname, int age, Genero gen, Categoria cat, boolean priv, double horas){
        super(name, lname, id, age, gen);
        horasSemanales = horas;
        categoriaProfesional = cat;
        trabajaEnPrivado = priv;
    }

    public Categoria getCategoriaProfesional() {
        //tipo enum categoria
        return categoriaProfesional;
    }

    public double getHorasSemanales() {
        return horasSemanales;
    }

    public boolean TrabajaEnPrivado() {
        return trabajaEnPrivado;
    }
}
