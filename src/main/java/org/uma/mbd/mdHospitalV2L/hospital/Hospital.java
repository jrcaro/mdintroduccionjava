package org.uma.mbd.mdHospitalV2L.hospital;

import java.util.ArrayList;
import java.util.List;

public class Hospital {
    private int numDepartamentos;
    private String nombre, direccion;
    List<Departamento> departamentos;
    List<Planta> plantas = new ArrayList<>();
    List<String> nombresDepartamentos;
    public Hospital(String name, String direcc, List<Departamento> dep, int nPlantas){
        nombre = name;
        direccion = direcc;
        departamentos = dep;
        numDepartamentos = dep.size();
        for (int i = 0; i < nPlantas; i++){
            plantas.add(new Planta(8, "P"+i));
        }

        nombresDepartamentos = new ArrayList<>();
        for (int i = 0; i < numDepartamentos; i++) {
            nombresDepartamentos.add(departamentos.get(i).getNombre());
        }
    }

    public Departamento getDepartamento(String name) {
        int i = 0;
        while(i < departamentos.size() && !name.equalsIgnoreCase(departamentos.get(i).getNombre())){
            i++;
        }

        if(i == departamentos.size()){
            throw new IndexOutOfBoundsException("Departamento no encontrado");
        }
        return departamentos.get(i);
    }

    public int getNumDepartamentos() {
        return numDepartamentos;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public int numPlantas() {
        return plantas.size();
    }

    public Planta getPlanta(int i) {
        if(i >= plantas.size()){
            throw new IndexOutOfBoundsException("Planta " + i + " no encontrada");
        }
        return plantas.get(i);
    }

    public List<String> getNombresDepartamentos() {
        return nombresDepartamentos;
    }

    @Override
    public String toString() {
        return "Hospital[" + departamentos + " " + numPlantas() + " plantas]";
    }
}
