package org.uma.mbd.mdHospitalV2L.hospital;

public class Cama {
    private String codigo;
    private Paciente paciente;
    public Cama(String cod){
        codigo = cod;
        paciente = null;
    }

    public String getCodigo() {
        return codigo;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente pac){
        paciente = pac;
    }

    public boolean estaLibre(){
        return paciente == null;
    }

    @Override
    public String toString(){
        String sal = "Cama: " + getCodigo();
        if(estaLibre()){
            sal += " libre";
        } else {
            sal += " ocupada por " + paciente;
        }
        return sal;
    }
}
