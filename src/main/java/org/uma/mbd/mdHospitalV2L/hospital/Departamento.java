package org.uma.mbd.mdHospitalV2L.hospital;

import java.util.List;

public class Departamento {
    private List<Medico> medicos;
    private String nombre;
    private int numMedicos;
    private String[] DNIsMedicos;
    public Departamento(String name, List<Medico> listMed){
        nombre = name;
        medicos = listMed;
        numMedicos = listMed.size();
        DNIsMedicos = new String[numMedicos];
        for (int i = 0; i < numMedicos; i++) {
            DNIsMedicos[i] = medicos.get(i).getDni();
        }
    }

    public Medico getMedico(String dni){
        int i = 0;
        while(i < medicos.size() && !dni.equalsIgnoreCase(medicos.get(i).getDni())){
            i++;
        }

        if(i == medicos.size()){
            throw new IndexOutOfBoundsException("El DNI no concuerda con nignun medico");
        }

        return medicos.get(i);
    }

    public boolean trabajaEnDepartamento(Medico med){
        return this.getMedico(med.getDni()) != null;
    }

    public int numMedicos(Categoria cat){
        int cont = 0;
        for (int i = 0; i < numMedicos; i++) {
            if(medicos.get(i).getCategoriaProfesional() == cat){
                cont++;
            }
        }
        return cont;
    }

    public int getNumMedicos(){
        return medicos.size();
    }

    public String getNombre() {
        return nombre;
    }

    public String[] getDNIsMedicos(){
        return DNIsMedicos;
    }

    @Override
    public String toString() {
        return "Departamento(" + nombre + "; " + medicos + ")";
    }
}
