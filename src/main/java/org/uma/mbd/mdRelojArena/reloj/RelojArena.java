package org.uma.mbd.mdRelojArena.reloj;

public class RelojArena {
    private int superior, inferior;
    public RelojArena(int a){inferior = a; superior = 0;}
    public void gira(){
        int temp = superior;
        superior = inferior;
        inferior = temp;
    }

    public void pasaTiempo(){
        inferior += superior;
        superior = 0;
    }

    public int getTiempoRestante(){return superior;}

    public void pasaTiempo(RelojArena reloj){ //simula el paso
                                            // de tiempo del argumento
        if(this.superior > reloj.superior){
            this.superior -= reloj.superior;
            this.inferior += reloj.superior;
            reloj.pasaTiempo();
        } else {
            this.pasaTiempo();
            reloj.pasaTiempo();
        }
    }

    public String toString() {
        return "R(" + superior +
                "/" + inferior + ")";
    }
}
