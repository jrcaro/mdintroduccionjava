package org.uma.mbd.mdRelojArena.reloj;

public class MedidorTiempo {
    private RelojArena izdo, drcho;
    private int tiempoTotal;
    public MedidorTiempo(int a, int b){
        izdo = new RelojArena(a);
        drcho = new RelojArena(b);
        tiempoTotal = 0;
    }

    public void giraIzquierdo(){
        izdo.gira();
        this.pasaTiempo();
    }

    public void giraDerecho(){
        drcho.gira();
        this.pasaTiempo();
    }
    public void giraAmbos(){
        izdo.gira();
        drcho.gira();
        this.pasaTiempo();
    }

    public void pasaTiempo(){
        if((izdo.getTiempoRestante() == 0) && (drcho.getTiempoRestante() != 0)){
            tiempoTotal += drcho.getTiempoRestante();
            izdo.pasaTiempo(drcho);
        } else if((drcho.getTiempoRestante() == 0) && (izdo.getTiempoRestante() != 0)) {
            tiempoTotal += izdo.getTiempoRestante();
            drcho.pasaTiempo(izdo);
        } else {
            if(drcho.getTiempoRestante() < izdo.getTiempoRestante()){
                tiempoTotal += drcho.getTiempoRestante();
                izdo.pasaTiempo(drcho);
            } else {
                tiempoTotal += izdo.getTiempoRestante();
                drcho.pasaTiempo(izdo);
            }
        }
    }

    public int getTiempoTotal(){
        return tiempoTotal;
    }

    @Override
    public String toString(){
        return "Izq " + izdo + " Drch " + drcho +
                " -> " + this.getTiempoTotal();
    }
}
