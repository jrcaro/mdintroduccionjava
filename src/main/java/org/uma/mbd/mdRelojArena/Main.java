package org.uma.mbd.mdRelojArena;

import org.uma.mbd.mdRelojArena.reloj.MedidorTiempo;
import org.uma.mbd.mdRelojArena.reloj.RelojArena;

public class Main {
    public static void main(String[] args) {
        MedidorTiempo mt = new MedidorTiempo(7, 5);
        mt.giraAmbos();
        System.out.println(mt);
        mt.giraDerecho();
        System.out.println(mt);
        mt.giraIzquierdo();
        System.out.println(mt);
        mt.giraDerecho();
        System.out.println(mt);
        mt.giraIzquierdo();
        System.out.println(mt);

    }
}
