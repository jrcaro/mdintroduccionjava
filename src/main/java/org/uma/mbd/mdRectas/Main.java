package org.uma.mbd.mdRectas;


import org.uma.mbd.mdRectas.rectas.Punto;
import org.uma.mbd.mdRectas.rectas.Recta;

public class Main { //clase
    public static void main(String [] args){ //siempre igual
        Recta r = new Recta(new Punto(2,3), new Punto(-1, 2));
        Recta r2 = r.perpendicularPor(new Punto(4,4));
        Punto pi = r.interseccionCon(r2);
        System.out.println(r);
        System.out.println(r2);
        System.out.println(pi);
        System.out.println(r.paralelaPor(new Punto(3, 4)));
    }
}
