package org.uma.mbd.mdInterfaces;

import org.uma.mbd.mdInterfaces.caso4.Persona;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Main5 {
    public static void main(String[] args) {
        Set<Persona> set = new HashSet<>(); //orden natural
        set.add(new Persona("juan", 25));
        set.add(new Persona("pedro", 12));
        set.add(new Persona("juan", 25));
        set.add(new Persona("luisa", 18));
        set.add(new Persona("andres", 12));
        System.out.println(set);

        Set<Persona> setO = new TreeSet<>(); //orden natural
        setO.add(new Persona("juan", 25));
        setO.add(new Persona("pedro", 12));
        setO.add(new Persona("juan", 25)); //eliminado porque el compareTo = 0
        setO.add(new Persona("luisa", 18));
        setO.add(new Persona("andres", 12));
        System.out.println(setO);

        Comparator<Persona> oE = (p1, p2) -> Integer.compare(p1.getEdad(), p2.getEdad());
        Comparator<Persona> oN = (p1, p2) -> p1.getNombre().compareTo(p2.getNombre());
        Set<Persona> setA = new TreeSet<>((oE.reversed().thenComparing(oN)).reversed());
        setA.addAll(setO);
        System.out.println(setA);

        Set<String> setS = new TreeSet<>(List.of("hola", "antonio","que","tal","estas","hoy"));
        System.out.println(setS);
        Comparator<String> oSize = (p1, p2) -> Integer.compare(p1.length(), p2.length());
        Set<String> setSort = new TreeSet<>(oSize.thenComparing(Comparator.naturalOrder()));
        setSort.addAll(setS);
        System.out.println(setSort);

    }
}
