package org.uma.mbd.mdInterfaces.caso4;

import java.util.Objects;

public class Persona implements Comparable<Persona>{

	private String nombre;
	private int edad;
	
	public Persona(String n, int e) {
		nombre = n;
		edad = e;
	}
	
	public String getNombre() {
		return nombre;
	}

	public int getEdad() {
		return edad;
	}

	@Override
	public boolean equals(Object o) {
		boolean res = o instanceof Persona;
		Persona p = res? (Persona)o : null;
		return res && nombre.equals(p.nombre) && edad == p.getEdad();
	}

	@Override
	public int hashCode() {
		return nombre.hashCode() + Integer.hashCode(edad);
	}

	@Override
	public String toString() {
		return nombre + ":" + edad;
	}

	@Override
	public int compareTo(Persona p) {
		int res = Integer.compare(edad, p.getEdad());
		if(res == 0){
			res = nombre.compareTo(p.getNombre());
		}
		return res;
	}
}









