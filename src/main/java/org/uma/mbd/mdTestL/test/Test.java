package org.uma.mbd.mdTestL.test;

public class Test {
    private int aciertos, errores;
    private String alumno;
    public Test(String name, int aciertos, int err){
        alumno = name;
        this.aciertos = aciertos;
        this.errores = err;
    }

    public int getAciertos() {
        return aciertos;
    }

    public int getErrores() {
        return errores;
    }

    public String getAlumno() {
        return alumno;
    }

    public double calificacion(double valAc, double valErr){
        if((valAc <= 0) || (valErr > 0)){
            throw new IllegalArgumentException("Valores no validos");
        }
        return aciertos*valAc + errores*valErr;
    }

    @Override
    public boolean equals(Object o){
        boolean res = o instanceof Test;
        Test t = res ? (Test)o : null;
        return res && (t.alumno.equalsIgnoreCase(alumno));
    }

    @Override
    public int hashCode(){
        return alumno.toLowerCase().hashCode() + aciertos + errores;
    }

    @Override
    public String toString() {
        return alumno + "[" + aciertos + ", " + errores + "]";
    }
}
