package org.uma.mbd.mdTestL.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestAsignatura {
    private String nombre;
    private double valorAciertos, valorErrores;
    private List<Test> examenes;
    private static final double APROBADO = 5;
    public TestAsignatura(String name, double valAc, double valErr, List<String> listaIn){
        nombre = name;
        valorAciertos = valAc;
        valorErrores = valErr;
        examenes = new ArrayList<>();
        extraeDatos(listaIn);
    }

    public TestAsignatura(String name, List<String> listaIn){
        this(name, 1, 0, listaIn);
    }

    public double notaMedia(){
        double nota = 0;
        for(int i = 0; i < examenes.size(); i++){
            nota += examenes.get(i).calificacion(valorAciertos, valorErrores);
        }

        return nota/examenes.size();
    }

    public int aprobados(){
        int cont = 0;
        for(int i = 0; i < examenes.size(); i++){
            if(examenes.get(i).calificacion(valorAciertos, valorErrores) >= APROBADO){
                cont++;
            }
        }

        return cont;
    }

    private void extraeDatos(List<String> listIn){
        String [] stringTemp;
        for (String datoAlumno : listIn){ //for each mas elegante
            //stringTemp = listIn.get(i).split("[:+]");
            try(Scanner sc = new Scanner(datoAlumno)){
                sc.useDelimiter("[:+]");
                Test test = new Test(sc.next(), sc.nextInt(),sc.nextInt());
                examenes.add(test);
            }
        }
    }

    public String getNombre() {
        return nombre;
    }
}
