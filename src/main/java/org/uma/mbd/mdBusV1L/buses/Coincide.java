package org.uma.mbd.mdBusV1L.buses;

public class Coincide implements Criterio {
    private Bus bus;
    public Coincide(Bus b){
        bus = b;
    }

    @Override
    public boolean esSeleccionable(Bus b) {
        return this.bus.equals(b);
    }

    @Override
    public String toString(){
        return "Autobuses " + bus;
    }
}
