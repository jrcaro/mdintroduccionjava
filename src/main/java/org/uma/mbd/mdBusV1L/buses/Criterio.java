package org.uma.mbd.mdBusV1L.buses;

@FunctionalInterface //solo tiene un metodo abstracto
public interface Criterio {
    boolean esSeleccionable(Bus bus);
}
