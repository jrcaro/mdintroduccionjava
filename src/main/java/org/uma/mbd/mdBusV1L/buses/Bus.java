package org.uma.mbd.mdBusV1L.buses;

public class Bus {
    private int codBus, codLinea;
    private String matricula;
    public Bus(int cod, String mat){
        codBus = cod;
        matricula = mat;
    }

    public void setCodLinea(int codLinea){
        this.codLinea = codLinea;
    }

    public int getCodBus() {
        return codBus;
    }

    public int getCodLinea() {
        return codLinea;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public boolean equals(Object o){
        boolean res = o instanceof Bus;
        Bus b = res ? (Bus)o : null;
        return res && (b.codBus == this.codBus) && (b.matricula.equalsIgnoreCase(this.matricula)); //usar mismas variables que el hashCode
    }

    @Override
    public int hashCode(){
        return matricula.toLowerCase().hashCode() + Integer.hashCode(codBus); //solo se puede sumar si es un entero OJO
    }

    @Override
    public String toString() {
        return "Bus(" + codBus + ", " + matricula + ", " + codLinea + ")";
    }
}
