package org.uma.mbd.mdBusV1L.buses;

import java.io.*;
import java.util.*;

public class Servicio {
    private  String ciudad;
    private List<Bus> buses;
    public Servicio(String name){
        ciudad = name;
        buses = new ArrayList<>();
    }

    public String getCiudad() {
        return ciudad;
    }

    public List<Bus> getBuses() {
        return buses;
    }

    public List<Bus> filtra(Criterio criterio){
        List<Bus> sal = new ArrayList<>();
        for (Bus bus : buses) {
            if (criterio.esSeleccionable(bus)) {
                sal.add(bus);
            }
        }
        return sal;
    }

    public void leeBuses(String fileStr) throws FileNotFoundException{
        try(Scanner sc = new Scanner(new File(fileStr))) {
            leeBuses(sc);
        }
    }

    private void leeBuses(Scanner sc){
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            stringToBus(line);
        }
        /*int cont = 0;
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            if(line.matches("[0-9]{3},[0-9]{4}[A-Z]{3},[0-9]{1,2}")){
                String [] sa = line.split(",");
                buses.add(new Bus(Integer.parseInt(sa[0]), sa[1]));
                buses.get(cont).setCodLinea(Integer.parseInt(sa[2]));
                cont++;
            } else {
                System.err.println("Error. Faltan datos o no son del tipo correcto " + line);
            }
        }*/
    }

    private void stringToBus(String datos) {
        try(Scanner sc = new Scanner(datos)){
            sc.useDelimiter(",");
            int codB = sc.nextInt();
            String mat = sc.next();
            int codL = sc.nextInt();
            Bus bus = new Bus(codB, mat);
            bus.setCodLinea(codL);
            buses.add(bus);
        } catch (InputMismatchException e){
            System.err.println("Dato incorrecto " + datos);
        } catch (NoSuchElementException e){
            System.err.println("Faltan datos " + datos);
        }
    }


    public void guarda(String fileStr, Criterio criterio) throws FileNotFoundException {
        try(PrintWriter pWriter = new PrintWriter(fileStr)) {
            guarda(pWriter, criterio);
        }
    }

    public void guarda(PrintWriter pw, Criterio criterio){
        List<Bus> buses = filtra(criterio);
        for (Bus bus: buses) {
            pw.println(bus.toString());
        }
    }
}
