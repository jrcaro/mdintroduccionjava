package org.uma.mbd.mdJarrasMezcla.jarrasMezcla;

public class Jarra {
    private final int capacidad; // una vez construido no se puede modificar
                                //(constante)
    private int contenido;
    public Jarra(int a) {
        if (a <= 0) {
            throw new IllegalArgumentException("Dato invalido");
        }
        capacidad = a;
        contenido = 0;
    }
    public int getCapacidad() {return capacidad;}
    public int getContenido() {return contenido;}
    public void llenar() {contenido = capacidad;}
    public void vaciar() {contenido = 0;}
    public void llenarDesde(Jarra j){
        if(this.contenido + j.contenido > this.capacidad){
            j.contenido = this.contenido + j.contenido - this.capacidad;
            this.contenido = this.capacidad;
        } else {
            this.contenido += j.contenido;
            j.contenido = 0;
        }
    }
    public String toString(){
        return "J(" + capacidad + ", " + contenido + ")";
    }
}
