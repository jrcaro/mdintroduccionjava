package org.uma.mbd.mdJarrasMezcla.jarrasMezcla;

public class JarraMezcla extends Jarra {
    private double pureza;
    private static final double AGUA = 0;
    private static final double VINO = 100;
    public JarraMezcla(int cap){
        super(cap);
        pureza = 0;
    }

    @Override
    public void llenar(){
        super.llenar();
        pureza = 0;
    }

    public void llenaVino(){
        super.llenar();
    }

    /*@Override
    public void llenarDesde(Jarra j){

    }*/
}
