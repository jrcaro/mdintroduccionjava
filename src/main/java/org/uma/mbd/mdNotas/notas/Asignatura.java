package org.uma.mbd.mdNotas.notas;

import java.io.*;
import java.util.*;


public class Asignatura {
	private String nombre;
	private List<Alumno> alumnos;
	private List<String> errores;

	private static final double APROBADO = 5;
	private static final String DNI = "[0-9]{8}[A-Z&&[^IOU]]";

	public Asignatura(String nombreAsignatura) {
		nombre = nombreAsignatura;
        alumnos = new ArrayList<>();
        errores = new ArrayList<>();
	}

	public void leeDatos(String nombreFichero) throws IOException {
		try (Scanner sc = new Scanner(new File(nombreFichero))){
			while (sc.hasNext()){
			    stringAAlumno(sc.nextLine());
            }
		}
	}

	public void leeDatos(String[] datos) {
		for (String dato: datos) {
			stringAAlumno(dato);
        }
	}

	private void stringAAlumno(String linea) {
		try (Scanner sc = new Scanner(linea)) {
			sc.useDelimiter("[;]+");
			sc.useLocale(Locale.ENGLISH);
			String id = sc.next();
			String name = sc.next();
			double nota = sc.nextDouble();
			if (!id.matches(DNI)) {
				throw new AlumnoException("DNI incorrecto: ");
			}
			alumnos.add(new Alumno(id, name, nota));
		}catch (InputMismatchException e) {
			//System.err.println("Dato incorrecto " + linea);
			errores.add("Nota no numerica: " + linea);
		} catch (NoSuchElementException e) {
			//System.err.println("Falta dato " + linea);
			errores.add("Falto dato: " + linea);
		} catch (AlumnoException e) {
		    //System.err.println("Nota incorrecta " + linea);
		    errores.add(e.getMessage() + linea);
		}
	}

	public double getCalificacion(Alumno al) throws AlumnoException {
		if(!alumnos.contains(al)){
			throw new AlumnoException("No existe el alumno " + al);
		}
		int i = alumnos.indexOf(al);
	    return alumnos.get(i).getCalificacion();
    }

	public List<Alumno> getAlumnos() {
		return alumnos;
	}

	public List<String> getErrores() {
		return errores;
	}

	public Set<String> getNombreAlumnos() {
		Set<String> nAlumno = new HashSet<>(); //Cambia TreeSet
        for (Alumno alumno: alumnos) {
            nAlumno.add(alumno.getNombre());
        }
		return nAlumno;
	}

	public Set<Alumno> getAlumnos(Comparator<Alumno> comp){
		Set<Alumno> sal = new TreeSet<>(comp);
		sal.addAll(alumnos);
		return sal;
	}

	public Map<Character, Set<Alumno>> getAlumnosInicial(){
		Comparator<Alumno> cCal = Comparator.comparingDouble(Alumno::getCalificacion);
		Comparator<Alumno> cNom = Comparator.comparing(Alumno::getNombre);
		Comparator<Alumno> cDNI = Comparator.comparing(Alumno::getDni);
		Comparator<Alumno> comp = cCal.reversed().thenComparing(cNom).thenComparing(cDNI);
		Map<Character, Set<Alumno>> map = new TreeMap<>();
		for (Alumno al : alumnos) {
			Character k = al.getNombre().charAt(0);
			Set<Alumno> set = map.computeIfAbsent(k, c -> new TreeSet<>(comp));
			set.add(al);
		}
		return map;
	}
	
	public double getMedia(CalculoMedia media) throws AlumnoException {
		return media.calcular(alumnos);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nombre);
		sb.append(alumnos);
		sb.append(errores);
		return sb.toString();
	}
}
