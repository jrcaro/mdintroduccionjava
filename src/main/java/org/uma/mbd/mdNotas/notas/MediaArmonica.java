package org.uma.mbd.mdNotas.notas;

import java.util.List;

public class MediaArmonica implements CalculoMedia{

	@Override
	public double calcular(List<Alumno> als) throws AlumnoException {
		double sumCal = 0;
		int k = 0;
		for (Alumno alumno : als) {
			if(alumno.getCalificacion() > 0) {
				sumCal += 1 / alumno.getCalificacion();
				k++;
			}
		}
		if(k == 0) throw new AlumnoException("Ningun alumno cumple el requisito");
		return k/sumCal;
	}
}
