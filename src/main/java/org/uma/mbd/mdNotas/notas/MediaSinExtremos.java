package org.uma.mbd.mdNotas.notas;

import java.util.List;

public class MediaSinExtremos implements CalculoMedia{
	private double min;
	private double max;
	
	public MediaSinExtremos(double min, double max){
		this.min = min;
		this.max = max;
	}

	@Override
	public double calcular(List<Alumno> als) throws AlumnoException{
		double media = 0;
		int i = 0;
		for (Alumno alumno : als) {
			if(alumno.getCalificacion() <= max && alumno.getCalificacion() >= min){
				media += alumno.getCalificacion();
				i++;
			}
		}
		if(i == 0) throw new AlumnoException("Ningun alumno cumple el requisito");
		return media/i;
	}
}
