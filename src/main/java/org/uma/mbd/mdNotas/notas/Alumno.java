package org.uma.mbd.mdNotas.notas;

public class Alumno implements Comparable<Alumno> {
	private String nombre;
	private String dni;
	private double nota;
	
	public Alumno(String d, String n, double c) throws AlumnoException {
		if(c < 0){
			throw new AlumnoException("Calificacion negativa: ");
		}
		nombre = n;
		dni = d;
		nota = c;
	}
	
	public Alumno(String d, String n) throws AlumnoException {
		this(d, n, 0);
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean res = obj instanceof Alumno;
		Alumno alum = res? (Alumno)obj : null;
		return res && nombre.equals(alum.getNombre()) && dni.equalsIgnoreCase(alum.getDni());
	}
	
	@Override
	public int hashCode() {
		return nombre.hashCode() + dni.hashCode();
	}

	@Override
	public int compareTo(Alumno al){
		int res = nombre.compareTo(al.getNombre());
		if(res == 0){
			res = dni.toLowerCase().compareTo(al.getDni().toLowerCase());
		}
		return res;
	}
	
	public String getNombre() {
		return nombre;
	}

	public String getDni() {
		return dni;
	}

	public double getCalificacion() {
		return nota;
	}
	
	@Override
	public String toString() {
		return nombre + " " + dni;
	}
}
