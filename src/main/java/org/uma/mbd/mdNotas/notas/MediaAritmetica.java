package org.uma.mbd.mdNotas.notas;

import java.util.List;

public class MediaAritmetica implements CalculoMedia {

	@Override
	public double calcular(List<Alumno> als) throws AlumnoException{
		if(als.isEmpty()) throw new AlumnoException("Lista vacia");
		double media = 0;
		for (Alumno alumno : als) {
			media += alumno.getCalificacion();
		}
		return media/als.size();
	}
}
