package org.uma.mbd.mdNotas;


import org.uma.mbd.mdNotas.notas.*;

import java.io.IOException;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;

public class Main2 {

	public static void main(String[] args) throws IOException {
		Asignatura algebra = new Asignatura("Algebra");

		try {
			algebra.leeDatos("src/main/java/org/uma/mbd/mdNotas/notas.txt");
			Alumno al1 = new Alumno("23322443k", "Lopez Turo, Manuel");
			Alumno al2 = new Alumno("342424f2J", "Fernandez Vara, Pedro");
			System.out.println("Calificacion de " + al1 + ": "
					+ algebra.getCalificacion(al1));
			System.out.println("Calificacion de " + al2 + ": "
					+ algebra.getCalificacion(al2));
		} catch (AlumnoException e) {
			System.err.println(e.getMessage());
		}
		try {
			CalculoMedia m1 = new MediaAritmetica();
			CalculoMedia m2 = new MediaArmonica();
			double min = 5.0;
			double max = 9.0;
			CalculoMedia m3 = new MediaSinExtremos(min,max);
			
			System.out.print("Media aritmetica ");
			System.out.println(algebra.getMedia(m1));
			System.out.print("Media armonica ");
			System.out.println(algebra.getMedia(m2));
			System.out.print("Media de valores en ["+min+","+max+"] ");
			System.out.println(algebra.getMedia(m3));
		} catch (AlumnoException e) {
			System.out.println("Error "+ e.getMessage());
		}
		System.out.println("Alumnos...");
		algebra.getAlumnos().forEach(
				alumno -> System.out.println(alumno + ": " + alumno.getCalificacion()));

		System.out.println("Malos...");
		algebra.getErrores().forEach(System.out::println);
		System.out.println("La asignatura completa");
		System.out.println(algebra);

		System.out.println();
		System.out.println("Nombre de los alumnos");
		Set<String> set0 = algebra.getNombreAlumnos();
		set0.forEach(System.out::println);

		System.out.println();
		System.out.println("Alumnos ordenados por notas, a igualdad de notas por nombre y a igualdad de nombres por dni");
		Comparator<Alumno> cCal = Comparator.comparingDouble(Alumno::getCalificacion);
		Comparator<Alumno> cNom = Comparator.comparing(Alumno::getNombre);
		Comparator<Alumno> cDNI = Comparator.comparing(Alumno::getDni);
		Comparator<Alumno> comp = cCal.thenComparing(cNom).thenComparing(cDNI);
		Set<Alumno> set1 = algebra.getAlumnos(comp);
		set1.forEach(alumno -> System.out.println(alumno + ": " + alumno.getCalificacion()));

		System.out.println();
		System.out.println("Alumnos ordenados por dni descendentes");
		Set<Alumno> set2 = algebra.getAlumnos(cDNI.reversed());
		set2.forEach(alumno -> System.out.println(alumno + ": " + alumno.getCalificacion()));

		System.out.println();
		System.out.println("Correspondencia de alumnos por inicial");
		Map<Character, Set<Alumno>> map = algebra.getAlumnosInicial();
		for (Character k : map.keySet()) {
			System.out.println("Letra " + k);
			Set<Alumno> set = map.get(k);
			for (Alumno al : set) {
				System.out.println("\t" + al + ": " + al.getCalificacion());
			}
		}
	}
}

