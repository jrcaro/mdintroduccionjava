package org.uma.mbd.mdTesoro;

import org.uma.mbd.mdTesoro.tesoro.Punto;
import org.uma.mbd.mdTesoro.tesoro.Tesoro;

public class Main {
    public static void main(String[] args) {
        Punto palR = new Punto(4,1);
        Punto palAm = new Punto(3,3);
        Punto palAz = new Punto(7,2);
        Tesoro tes = new Tesoro(palR, palAm, palAz);
        System.out.println(tes.getTesoro());

        tes.setPalmeraR(new Punto(5,5));
        System.out.println(tes.getTesoro());
    }
}
