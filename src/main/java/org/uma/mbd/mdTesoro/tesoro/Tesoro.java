package org.uma.mbd.mdTesoro.tesoro;

public class Tesoro {
    private Punto palmeraAm, palmeraAz, palmeraR, estacaAm, estacaAz, tesoro;
    public Tesoro(Punto x, Punto y, Punto z){
        palmeraR = x;
        palmeraAm = y;
        palmeraAz = z;
        this.calculaPosiciones();
    }

    private void calculaPosiciones(){
        Vector vAmarillo = new Vector(palmeraR, palmeraAm);
        Vector vEstacaAm = vAmarillo.ortogonal();
        estacaAm = new Punto(palmeraAm.getX(), palmeraAm.getY());
        estacaAm.trasladar(vEstacaAm.getComponenteX(), vEstacaAm.getComponenteY());

        Vector vAzul = new Vector(palmeraR, palmeraAz);
        Vector vEstacaAz = vAzul.ortogonal();
        vEstacaAz = new Vector(-vEstacaAz.getComponenteX(), -vEstacaAz.getComponenteY());
        estacaAz = new Punto(palmeraAz.getX(), palmeraAz.getY());
        estacaAz.trasladar(vEstacaAz.getComponenteX(), vEstacaAz.getComponenteY());

        Vector vTesoro = new Vector(estacaAm, estacaAz);
        tesoro = new Punto(estacaAm.getX(), estacaAm.getY());
        tesoro.trasladar(vTesoro.getComponenteX()/2, vTesoro.getComponenteY()/2);
    }

    public void setPalmeraAm(Punto palmeraAm) {
        this.palmeraAm = palmeraAm;
        this.calculaPosiciones();
    }

    public void setPalmeraAz(Punto palmeraAz) {
        this.palmeraAz = palmeraAz;
        this.calculaPosiciones();
    }

    public void setPalmeraR(Punto palmeraR) {
        this.palmeraR = palmeraR;
        this.calculaPosiciones();
    }

    public Punto getEstacaAm() {
        return estacaAm;
    }

    public Punto getEstacaAz() {
        return estacaAz;
    }

    public Punto getTesoro() {
        return tesoro;
    }
}
