package org.uma.mbd.mdLibreriaV1L.libreria;

import java.util.ArrayList;
import java.util.List;

public class Libreria {
    private List<Libro> libros;
    public Libreria(){
        libros = new ArrayList<>(); //se puede quitar este construcotr y
                                    //declararlo en la variable
    }

    public void addLibro(String aut, String tit, double p){
        Libro libro = new Libro(aut,tit,p);
        addLibro(libro);
    }

    private int posicionLibro(String aut, String tit){
        int i = 0;
        while(i < libros.size() &&
                !(aut.equalsIgnoreCase(libros.get(i).getAutor()) &&
                        tit.equalsIgnoreCase(libros.get(i).getTitulo()))){
            i++;
        }
        return (i == libros.size())? -1 : i; //operador ternario
    }

    private void addLibro(Libro l){
        int i = posicionLibro(l.getAutor(), l.getTitulo());
        if(i < 0) {
            libros.add(l);
        } else {
            libros.add(i, l);
        }
    }

    public void remLibro(Libro l){
        int i = posicionLibro(l.getAutor(), l.getTitulo());
        if(i >= 0){
            libros.remove(i);
        }
    }

    public double getPrecioBase(Libro l){
        int i = posicionLibro(l.getAutor(), l.getTitulo());
        return (i >= 0)? libros.get(i).getPrecioBase() : 0;
    }

    public double getPrecioFinal(Libro l){
        int i = posicionLibro(l.getAutor(), l.getTitulo());
        return (i >= 0)? libros.get(i).getPrecioFinal() : 0;
    }

    @Override
    public String toString(){
        return libros.toString();
    }
}
