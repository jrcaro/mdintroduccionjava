package org.uma.mbd.mdZonasMusculacion.musculacion;

public class Maquina  implements Comparable<Maquina>{
    private int maquinaId, nivel;
    private String nombre, urlImagen, funcion, descripcion;
    public Maquina(int id, String name){
        nombre = name;
        maquinaId = id;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getMaquinaId() {
        return maquinaId;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public int compareTo(Maquina maq){ //si se hace el compare es necesario el equals y el hash??
        return Integer.compare(maquinaId, maq.getMaquinaId());
    }

    @Override
    public String toString(){
        return "Maquina(" + maquinaId + ", " + nombre + ")";
    }
}
