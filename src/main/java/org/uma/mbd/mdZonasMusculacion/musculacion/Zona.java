package org.uma.mbd.mdZonasMusculacion.musculacion;

import java.util.Set;
import java.util.TreeSet;

public class Zona {
    private int zonaId;
    private String nombre, urlImagen;
    private double longitud, latitud;
    private Set<Maquina> maquinas;
    public Zona(int id, String name){
        zonaId = id;
        nombre = name;
        maquinas = new TreeSet<>();
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public int getZonaId() {
        return zonaId;
    }

    public String getNombre() {
        return nombre;
    }

    public Set<Maquina> getMaquinas() {
        return maquinas;
    }

    public int compareTo(Zona z){
        return Integer.compare(zonaId, z.getZonaId());
    }

    public void agrega(Maquina mq){
        maquinas.add(mq);
    }

    @Override
    public String toString(){
        return "Zona( " + zonaId + ", " + nombre + ")";
    }
}
