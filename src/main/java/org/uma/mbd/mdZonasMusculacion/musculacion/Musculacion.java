package org.uma.mbd.mdZonasMusculacion.musculacion;


import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.*;
import java.net.URL;
import java.util.*;

public class Musculacion {
    private final static int MAQUINA_ID = 18;
    private final static int MAQUINA_NOMBRE = 19;
    private final static int MAQUINA_URL_ICON = 21;
    private final static int MAQUINA_NIVEL = 20;
    private final static int MAQUINA_FUNCION = 22;
    private final static int MAQUINA_DESCRIPCION = 23;
    private final static int ZONA_ID = 14;
    private final static int ZONA_NOMBRE = 15;
    private final static int ZONA_URL_ICON = 17;
    private final static int ZONA_UBICACION = 2;

    private String ciudad;
    //private Set<Zona> zonas;
    private Map<Integer, Zona> zonas;

    public Musculacion(String ciudad) {
        this.ciudad = ciudad;
        zonas = new TreeMap<>();
    }

    public String getCiudad() {
        return ciudad;
    }

    public void leeDatosLocal(String ficheroCSV) throws IOException {
        try (InputStream in = new FileInputStream(ficheroCSV);
             InputStreamReader isr = new InputStreamReader(in);
             BufferedReader bin = new BufferedReader(isr);
             CSVReader reader = new CSVReader(bin)) {
            leeDatos(reader);
        }
    }

    public void leeDatosUrl(String urlCSV) throws IOException {
        URL url = new URL(urlCSV);
        try (InputStream in = url.openStream();
             InputStreamReader isr = new InputStreamReader(in);
             BufferedReader bin = new BufferedReader(isr);
             CSVReader reader = new CSVReader(bin)) {
            leeDatos(reader);
        }
    }

    private void leeDatos(CSVReader reader) throws IOException {
        try {
            reader.readNext(); // Ignoramos la primera linea
            List<String[]> datos = reader.readAll();  // Se leen todos los datos en un array
            // El resultado es una lista de arrays.
            // Cada array contiene todos los tokens de una linea

            for (String[] tokens : datos) { // Foreach por cada array de tokens
                // Leemos los datos que nos interesan de la maquina
                Zona zona = tokensAZona(tokens);
                int zonaId = Integer.parseInt(tokens[ZONA_ID]);
                zonas.putIfAbsent(zonaId, zona);
                // leemos el identificador de la zona

                // Se debe ver si la zona existe.
                // Si existe no se hace nada
                // Si no existe se debe crear con todos sus datos
                // y almacenar en la correspondencia asociada a su id

                // COMPLETAR

                // Por último, se debe agregar la máquina a esta zona
                // COMPLETAR
            }
        } catch (CsvException e) {
            System.err.println(e);
        }
    }

    private Maquina tokensAMaquina(String [] line){
        int maqId = Integer.parseInt(line[MAQUINA_ID]);
        String nombre = line[MAQUINA_NOMBRE];
        Maquina mq = new Maquina(maqId, nombre);
        mq.setDescripcion(line[MAQUINA_DESCRIPCION]);
        mq.setFuncion(line[MAQUINA_FUNCION]);
        mq.setNivel(Integer.parseInt(line[MAQUINA_NIVEL]));
        mq.setUrlImagen(line[MAQUINA_URL_ICON]);
        return mq;
    }

    private Zona tokensAZona(String [] line){
        int zonaId = Integer.parseInt(line[ZONA_ID]);
        String nombre = line[ZONA_NOMBRE];
        Zona zona = new Zona(zonaId, nombre);
        String firstStep = line[ZONA_UBICACION].split("\\(")[1];
        String[] secStep = firstStep.split(" ");
        zona.setLatitud(Double.parseDouble(secStep[0]));
        zona.setLongitud(Double.parseDouble(secStep[1].replace(")", "")));
        zona.setUrlImagen(line[ZONA_URL_ICON]);
        zona.agrega(tokensAMaquina(line));
        return zona;
    }

    public Set<Zona> getZonas() {
        Set<Zona> set = new TreeSet<>();
        for (Zona zona : zonas.values()) {
            set.add(zona);
        }
        return set;
    }

    public Set<Maquina> getMaquinasEnZonaId(int zonaId) {
        return zonas.get(zonaId).getMaquinas();
    }

    public Set<Zona> getZonasConMaquinaId(int mkId) {
        return null;
    }
}
