package org.uma.mbd.mdCoche.coches;

public class CocheImportado extends Coche {
    private double homologacion;
    public CocheImportado(String name, double price, double hom) {
        super(name, price);
        homologacion = hom;
    }

    @Override
    public double precioTotal(){
        return super.precioTotal() + homologacion;
    }
}
