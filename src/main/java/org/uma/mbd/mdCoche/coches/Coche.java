package org.uma.mbd.mdCoche.coches;

public class Coche {
    private String nombre;
    private double precio;
    private static double PIVA = 0.16; //variable de clase
    public Coche(String name, double price){
        nombre = name;
        precio = price;
    }

    public static void setPIVA(double i) {
        PIVA = i;
    }

    public double precioTotal(){
        return precio + precio*PIVA/100;
    }

    @Override
    public String toString(){
        return nombre + " -> " + this.precioTotal();
    }
}
