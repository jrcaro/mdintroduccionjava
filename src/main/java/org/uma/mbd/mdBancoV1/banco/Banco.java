package org.uma.mbd.mdBancoV1.banco;

import java.util.Arrays;

public class Banco {
    private static final int TAM_ARRAY_CTAS = 10; //orden static final??
    private static final int PRIMER_NUM_CTA = 1001;
    private String nombre;
    private int ppl, snc;
    private Cuenta [] ctas;
    public Banco(String name, int tam){
        nombre = name;
        snc = PRIMER_NUM_CTA;
        ppl = 0;
        ctas = new Cuenta[tam];
    }
    public Banco(String name){
        this(name, TAM_ARRAY_CTAS);
    }

    public int abrirCuenta(String name, double dinero){
        if(ppl == ctas.length){
            ctas = Arrays.copyOf(ctas, ppl*2);
        }
        ctas[ppl] = new Cuenta(name, snc, dinero);
        ppl++;
        snc++;
        return snc-1;
    }

    public int abrirCuenta(String name){
        return this.abrirCuenta(name, 0);
        /*if(ppl == ctas.length){
            ctas = Arrays.copyOf(ctas, ppl*2);
        }
        ctas[ppl] = new Cuenta(name, snc, 0);
        ppl++;
        snc++;
        return snc-1;*/
    }

    public void cerrarCuenta(int nCuenta){
        int i = posicionCuenta(nCuenta);
        for(int j = i; j < ppl-1; j++){
            ctas[j] = ctas[j+1];
        }
        ppl--;
    }

    private int posicionCuenta(int nCuenta){
        int i = 0;
        while((i < ppl) && !(ctas[i].getNumCuenta() == nCuenta)){
            i++;
        }

        if(i == ppl){
            throw  new RuntimeException("No existe la cuenta dada");
        }
        return i;
    }

    public void ingreso(int nCuenta, double dinero){
        int i = posicionCuenta(nCuenta);
        ctas[i].ingreso(dinero);
    }

    public  void debito(int nCuenta, double dinero) {
        int i = posicionCuenta(nCuenta);
        if(ctas[i].getSaldo() < dinero){
            ctas[i].debito(ctas[i].getSaldo());
        } else {
            ctas[i].debito(dinero);
        }

    }

    public double saldo(int nCuenta){
        int i = posicionCuenta(nCuenta);
        return ctas[i].getSaldo();
    }

    public void transferencia(int nCuenta1, int nCuenta2, double dinero){ //return double??
        int i = posicionCuenta(nCuenta1);
        int j = posicionCuenta(nCuenta2);
        ctas[j].ingreso(dinero);
        ctas[i].debito(dinero);
    }

    @Override
    public String toString(){
        String salida = nombre + ": [";
        for(int i = 0; i < ppl; i++){
            salida += ctas[i].toString();
        }
        return salida + "]";
    }
}
