package org.uma.mbd.mdBancoV2L.banco;

public class Cuenta {
    private int numCuenta;
    private double saldo;
    private String titular;
    private static final double INTERES = 0.02;
    public Cuenta(String tit, int nCuenta, double sal){
        titular = tit;
        numCuenta = nCuenta;
        saldo = sal;
    }

    public Cuenta(String tit, int nCuenta){
        this(tit, nCuenta, 0);
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public String getTitular() {
        return titular;
    }

    public void ingreso(double dinero){
        saldo += dinero;
    }

    public void debito(double dinero){
        saldo -= dinero;
    }

    public void cierreEjercicio(){
        saldo += saldo*INTERES;
    }

    @Override
    public String toString() {
        return "[(" + titular + "/"+ numCuenta + " -> " +
                saldo +  "]";
    }
}
