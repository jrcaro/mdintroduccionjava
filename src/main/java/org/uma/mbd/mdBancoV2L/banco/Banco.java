package org.uma.mbd.mdBancoV2L.banco;

import java.util.ArrayList;
import java.util.List;

public class Banco {
    private static final int PRIMER_NUM_CTA = 1001;
    private String nombre;
    private int snc;
    private List<Cuenta> ctas;
    public Banco(String name){
        nombre = name;
        snc = PRIMER_NUM_CTA;
        ctas = new ArrayList<>();
    }

    public int abrirCuenta(String name, double dinero){
        if(dinero < 0){
            throw new BancoException("Saldo inicial invalido");
        }
        ctas.add(new Cuenta(name, snc, dinero));
        snc++;
        return snc-1;
    }

    public int abrirCuenta(String name){
        return this.abrirCuenta(name, 0);
    }

    public int abrirCuentaVip(String tit, double saldoInicial, double incentivo){
        if((saldoInicial < 0) || (incentivo < 0)){
            throw new BancoException("Saldo inicial o incentivo invalido");
        }
        ctas.add(new CuentaVip(tit, saldoInicial, snc, incentivo));
        snc++;
        return snc-1;
    }

    public List<Integer> abrirCuentas(List<String> titulares){
        List<Integer> salida = new ArrayList<>();
        for(int i = 0; i < titulares.size(); i++){
            ctas.add(new Cuenta(titulares.get(i), snc, 0));
            salida.add(snc);
            snc++;
        }
        return salida;
    }

    public void cerrarCuenta(int nCuenta){
        ctas.remove(posicionCuenta(nCuenta));
    }

    public void cerrarCuentas(List<Integer> numCuentas){
//            numCuentas.forEach(cta -> {
//                try {
//                    ctas.remove(posicionCuenta(numCuentas.get(i)));
//                }catch (RuntimeException e){
//
//                }
//            }); Stream
        for (int i = 0; i < numCuentas.size(); i++) {
            try {
                ctas.remove(posicionCuenta(numCuentas.get(i)));
            }catch (RuntimeException e){

            }
        }
    }

    public void cierreEjercicio(){
        for(int i = 0; i < ctas.size(); i++){
            ctas.get(i).cierreEjercicio();
        }
    }

    private int posicionCuenta(int nCuenta){
        int i = 0;
        while((i < ctas.size()) && !(ctas.get(i).getNumCuenta() == nCuenta)){
            i++;
        }

        if(i == ctas.size()){
            throw new BancoException("No existe la cuenta dada");
        }
        return i;
    }

    public void ingreso(int nCuenta, double dinero){
        if(dinero < 0){
            throw new BancoException("Dinero invalido");
        }
        ctas.get(posicionCuenta(nCuenta)).ingreso(dinero);
    }

    public  void debito(int nCuenta, double dinero) {
        if(dinero < 0){
            throw new BancoException("Dinero invalido");
        }
        int i = posicionCuenta(nCuenta);
        if(ctas.get(i).getSaldo() < dinero){
            ctas.get(i).debito(ctas.get(i).getSaldo());
        } else {
            ctas.get(i).debito(dinero);
        }

    }

    public double saldo(int nCuenta){
        int i = posicionCuenta(nCuenta);
        return ctas.get(i).getSaldo();
    }

    public void transferencia(int nCuenta1, int nCuenta2, double dinero){ //return double??
        if(dinero < 0){
            throw new BancoException("Dinero invalido");
        }
        int i = posicionCuenta(nCuenta2);
        int j = posicionCuenta(nCuenta1);
        ctas.get(i).ingreso(dinero);
        ctas.get(j).debito(dinero);
    }

    @Override
    public String toString(){
        return nombre + ctas.toString();
    }
}
