package org.uma.mbd.mdBancoV2L.banco;

public class CuentaVip extends Cuenta {
    private double incentivo;
    public CuentaVip(String tit, double s, int n, double incentivo){
        super(tit, n, s);
        this.incentivo = incentivo;
    }

    @Override
    public void cierreEjercicio(){
        super.cierreEjercicio(); //??
        super.ingreso(incentivo);
    }

    @Override
    public String toString() {
        return super.toString() + "$" + incentivo + "$";
    }
}
