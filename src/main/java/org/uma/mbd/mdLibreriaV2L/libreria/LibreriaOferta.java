package org.uma.mbd.mdLibreriaV2L.libreria;

public class LibreriaOferta extends Libreria {
    private String [] oferta;
    private double descuento;
    public LibreriaOferta(double desc, String[] ofer){
        super();
        setOferta(desc, ofer);
    }

    public void setOferta(double desc, String[] ofer){
        oferta = ofer;
        descuento = desc;
    }

    public String[] getOferta(){
        return oferta;
    }

    public double getDescuento(){
        return descuento;
    }

    @Override
    public void addLibro(String aut, String tit, double pBase){
        Libro lib;
        if(this.esAutorEnOferta(aut)){ //otra forma de hacerlo
            lib = new LibroEnOferta(aut,tit, pBase, descuento); //vinculacion dinamica
            //super.addLibro(libOfer);
        } else {
            lib = new Libro(aut, tit, pBase);
            //super.addLibro(lib);
        }
        super.addLibro(lib);
    }

    private boolean esAutorEnOferta(String aut){
        int i = 0;
        while(i < oferta.length && !oferta[i].equalsIgnoreCase(aut)){
            i++;
        }
        return i != oferta.length;
    }

    @Override
    public String toString(){
        String salida = descuento + "% [";
        //se puede sustituir por Arrays.toString()
        for(int i = 0; i < oferta.length; i++){
            salida += oferta[i];
            if(i < oferta.length - 1){
                salida += "; ";
            }
        }
        return salida + "] " + super.toString();
    }
}
