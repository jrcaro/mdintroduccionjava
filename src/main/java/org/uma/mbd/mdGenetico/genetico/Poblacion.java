package org.uma.mbd.mdGenetico.genetico;

public class Poblacion {
    private Individuo[] individuos;
    private int numIndividuos;
    public Poblacion(int tamPob, int longitud, Problema problema){
        if((tamPob < 0) || (longitud < 0)){
            throw new IllegalArgumentException("Poblacion o longitud no valida");
        }
        numIndividuos = tamPob;
        individuos = new Individuo[numIndividuos];
        for(int i = 0; i < numIndividuos; i++){
            individuos[i] = new Individuo(longitud, problema);
        }
    }

    public Individuo getIndividuos(int j) {
        if(j >= individuos.length){
            throw new IndexOutOfBoundsException("Posicion no valida");
        }
        return individuos[j];
    }

    public int getNumIndividuos() {
        return individuos.length;
    }

    public Individuo mejorIndividuo(){
        int j = 0;
        double fit = 0;
        for(int k = 0; k < numIndividuos; k++){
            if(individuos[k].getFitness() > fit){
                fit = individuos[k].getFitness();
                j = k;
            }
        }
        return individuos[j];
    }

    private int peorIndividuo(){ //indice del peor individuo
        int j = 0;
        double fit = mejorIndividuo().getFitness();
        for(int k = 0; k < numIndividuos; k++){
            if(individuos[k].getFitness() < fit){
                fit = individuos[k].getFitness();
                j = k;
            }
        }
        return j;
    }

    public void reemplaza(Individuo ind){
        int j = this.peorIndividuo();
        if(this.individuos[j].getFitness() < ind.getFitness()){
            this.individuos[j] = ind;
        }
    }
}
