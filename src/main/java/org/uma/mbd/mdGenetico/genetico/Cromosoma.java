package org.uma.mbd.mdGenetico.genetico;

import java.util.Random;

public class Cromosoma {
    private int longitud;
    protected int[] datos;
    protected static Random gna = new Random();
    protected static int GEN_POR_DEFECTO = 0;
    public Cromosoma(int longitud, boolean ini){
        if(longitud <= 0){
            throw new IllegalArgumentException("Longitud no valida");
        }
        this.longitud = longitud;
        datos = new int[longitud];

        for(int i = 0; i < datos.length; i++){
            if(ini){
                datos[i] = gna.nextInt(2); //numeros aleatorios entre 0 y 1
            } else {
                datos[i] = GEN_POR_DEFECTO;
            }
        }
    }

    public int getGen(int i){
        if(i >= longitud){
            throw new IndexOutOfBoundsException("Posicion no valida");
        }
        return datos[i];
    }

    public void setGen(int i, int val){
        if(i >= longitud){
            throw new IndexOutOfBoundsException("Posicion no valida");
        } else if ((val != 0) && (val != 1)){
            throw new IllegalArgumentException("Valores no validos");
        }
        datos[i] = val;
    }

    public int getLongitud(){
        return datos.length;
    }

    public void mutar(double probMutar){
        if ((probMutar < 0) && (probMutar > 1)){
            throw new IllegalArgumentException("Valores no validos");
        }
        for (int i = 0; i < datos.length; i++) {
            if(gna.nextDouble() < probMutar) {
                datos[i] ^= 1;
            }
        }
    }

    public Cromosoma copia(){
        Cromosoma copy = new Cromosoma(getLongitud(), false);
        for(int i = 0; i < datos.length; i++){
            copy.datos[i] = this.datos[i];
        }
        return copy;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < datos.length - 1; i++) {
            s.append(datos[i]).append(", ");
        }
        return "[" + s + datos[datos.length - 1] + "]";
    }
}
