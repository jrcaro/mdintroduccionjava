package org.uma.mbd.mdBusV2;

import org.uma.mbd.mdBusV2.buses.Criterio;
import org.uma.mbd.mdBusV2.buses.EnMatricula;
import org.uma.mbd.mdBusV2.buses.PorLinea;
import org.uma.mbd.mdBusV2.buses.Servicio;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class MainPrueba {
    public static void main(String [] args) {
        Servicio servicio = new Servicio("Malaga");
        String path = "src/main/java/org/uma/mbd/mdBusV1L/";
        try {
            System.out.println(servicio.getCiudad());
            servicio.leeBuses(path + "buses.txt");
            Criterio cr1 = new PorLinea(21);
            servicio.guarda(path + "linea21.txt", cr1);
            servicio.guarda(new PrintWriter(System.out,true), cr1);
            Criterio cr2 = new EnMatricula("29");
            servicio.guarda(path + "contiene29.txt", cr2);
            servicio.guarda(new PrintWriter(System.out,true), cr2);
        } catch (FileNotFoundException e) {
            System.err.println("No existe el fichero de entrada");
        }
    }
}
