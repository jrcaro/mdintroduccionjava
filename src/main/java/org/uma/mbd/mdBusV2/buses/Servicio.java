package org.uma.mbd.mdBusV2.buses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class Servicio {
    private  String ciudad;
    private Set<Bus> buses;
    public Servicio(String name){
        ciudad = name;
        buses = new HashSet<>();
    }

    public String getCiudad() {
        return ciudad;
    }

    public Set<Bus> getBuses() {
        return buses;
    }

    public Set<Bus> filtra(Criterio criterio){//, Comparator<Bus> compBus){
        Set<Bus> sal = new HashSet<>();
        for (Bus bus : buses) {
            if (criterio.esSeleccionable(bus)) {
                sal.add(bus);
            }
        }
        //compBus.
        return sal;
    }

    public void leeBuses(String fileStr) throws FileNotFoundException{
        try(Scanner sc = new Scanner(new File(fileStr))) {
            leeBuses(sc);
        }
    }

    private void leeBuses(Scanner sc){
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            stringToBus(line);
        }
    }

    private void stringToBus(String datos) {
        try(Scanner sc = new Scanner(datos)){
            sc.useDelimiter(",");
            int codB = sc.nextInt();
            String mat = sc.next();
            int codL = sc.nextInt();
            Bus bus = new Bus(codB, mat);
            bus.setCodLinea(codL);
            buses.add(bus);
        } catch (InputMismatchException e){
            System.err.println("Dato incorrecto " + datos);
        } catch (NoSuchElementException e){
            System.err.println("Faltan datos " + datos);
        }
    }


    public void guarda(String fileStr, Criterio criterio) throws FileNotFoundException {
        try(PrintWriter pWriter = new PrintWriter(fileStr)) {
            guarda(pWriter, criterio);
        }
    }

    public void guarda(PrintWriter pw, Criterio criterio){
        Set<Bus> buses = filtra(criterio);
        for (Bus bus: buses) {
            pw.println(bus.toString());
        }
    }
}
