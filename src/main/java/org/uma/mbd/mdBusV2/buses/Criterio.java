package org.uma.mbd.mdBusV2.buses;

@FunctionalInterface //solo tiene un metodo abstracto
public interface Criterio {
    boolean esSeleccionable(Bus bus);
}
