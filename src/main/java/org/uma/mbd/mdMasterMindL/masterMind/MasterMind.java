package org.uma.mbd.mdMasterMindL.masterMind;

import java.util.*;

public class MasterMind {
    private static final int TAMANO_POR_DEFECTO = 4;
    private static final Random alea = new Random();
    private int longitud;
    private String secreto;
    public MasterMind(){
        this(TAMANO_POR_DEFECTO);
    }

    public MasterMind(int n){
        if(n > 9 || n < 1){
            throw new MasterMindException("Tamano no valido " + n);
        }
        longitud = n;
        generaCombinacionSecreta(n);
    }

    public MasterMind(String s){
        longitud = s.length();
        if(!validaCombinancion(s)){
            throw new MasterMindException("Cifra no valida " + s);
        }
        secreto = s;
    }

    private void generaCombinacionSecreta(int n){
        secreto = "";
        while(secreto.length() < n){
            int cifra = alea.nextInt(10);
            String cifraStr = Integer.toString(cifra);
            if(!secreto.contains(cifraStr)){
                secreto += cifraStr;
            }
        }
        /*int cont = 0;
        List<Integer> p = new ArrayList<>();
        p.add(alea.nextInt(10));
        while(cont < n-1){
            int t;
            do {
                t = alea.nextInt(10);
            } while(p.contains(t));
            p.add(t);
            cont++;
        }
        String sal = "";
        for(Integer cifra : p){
            sal += cifra;
        }
        secreto = sal;*/
    }

    protected boolean validaCombinancion(String cifras){
        boolean valido = cifras.length() == longitud;
        if(valido){
            valido = cifras.matches("[0-9]+");
        }
        if(valido){
            int i = 0;
            while(valido && i < longitud - 1){
                String cifraStr = "" + cifras.charAt(i);
                valido = !cifras.substring(i+1).contains(cifraStr);
                i++;
            }
        }

        return valido;
    }

    public Movimiento intento(String cifras){
        if(!validaCombinancion(cifras)){
            throw new MasterMindException("Cifras no validas " + cifras);
        }
        int col = 0;
        int des = 0;
        for (int i = 0; i < longitud; i++) {
            if(cifras.charAt(i) == secreto.charAt(i)){
                col++;
            } else if (secreto.indexOf(cifras.charAt(i)) >= 0 ){
                des++;
            }
        }

        return new Movimiento(cifras, col, des);
    }

    public int getLongitud() {
        return longitud;
    }

    public String getSecreto() {
        return secreto;
    }
}
