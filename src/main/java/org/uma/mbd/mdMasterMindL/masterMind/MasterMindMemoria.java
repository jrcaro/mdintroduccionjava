package org.uma.mbd.mdMasterMindL.masterMind;

import java.util.ArrayList;
import java.util.List;

public class MasterMindMemoria extends MasterMind {
    private List<Movimiento> movimientos = new ArrayList<>();
    public MasterMindMemoria(){
        super();
    }

    public MasterMindMemoria(int n){
        super(n);
    }

    public MasterMindMemoria(String cifras){
        super(cifras);
    }

    @Override
    public Movimiento intento(String cifras){
        Movimiento mov = super.intento(cifras);
        if(movimientos.contains(mov)){
            throw new MasterMindException("Movimiento ya realizado");
        }
        movimientos.add(mov);
        return mov;
    }

    public List<Movimiento> movimientos(){
        return movimientos;
    }
}
