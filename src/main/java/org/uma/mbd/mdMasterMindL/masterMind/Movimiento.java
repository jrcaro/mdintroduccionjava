package org.uma.mbd.mdMasterMindL.masterMind;

public class Movimiento {
    private int colocadas, descolocadas;
    private String cifras;
    public Movimiento(String dato, int col, int des){
        colocadas = col;
        descolocadas = des;
        cifras = dato;
    }

    public int getColocadas() {
        return colocadas;
    }

    public int getDescolocadas() {
        return descolocadas;
    }

    public String getCifras() {
        return cifras;
    }

    @Override
    public boolean equals(Object o){
        boolean res = o instanceof Movimiento;
        Movimiento t = res ? (Movimiento)o : null;
        return res && this.cifras.equals(t.cifras);
    }

    @Override
    public int hashCode(){
        return cifras.hashCode();
    }

    @Override
    public String toString() {
        return "[" + cifras + ", " + colocadas + ", " + descolocadas + "]";
    }
}
