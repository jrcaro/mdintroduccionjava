package org.uma.mbd.mdAlturas.alturas;

public class MayoresQue implements Filtra {
    private double altMin;
    public MayoresQue(double min){
        altMin = min;
    }


    @Override
    public boolean test(Pais pais) {
        return pais.getAltura() >= altMin;
    }
}
