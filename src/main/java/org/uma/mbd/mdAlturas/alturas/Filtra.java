package org.uma.mbd.mdAlturas.alturas;

@FunctionalInterface
public interface Filtra {
    boolean test (Pais pais);
}
