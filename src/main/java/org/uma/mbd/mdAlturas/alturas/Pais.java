package org.uma.mbd.mdAlturas.alturas;

public class Pais implements Comparable<Pais>{
    private String nombre, continente;
    private double altura;
    public Pais(String name, String cont, double alt){
        nombre = name;
        continente = cont;
        altura = alt;
    }

    public String getNombre() {
        return nombre;
    }

    public String getContinente() {
        return continente;
    }

    public double getAltura() {
        return altura;
    }

    @Override
    public boolean equals(Object o){
        boolean res = o instanceof Pais;
        Pais p = res? (Pais)o : null;
        return res && nombre.equalsIgnoreCase(((Pais) o).getNombre());
    }

    @Override
    public int hashCode(){
        return nombre.toLowerCase().hashCode();
    }

    @Override
    public int compareTo(Pais p){
        int res = continente.compareTo(p.continente);
        if(res == 0){
            res = nombre.compareTo(p.getNombre());
        }
        return res;
    }

    @Override
    public String toString() {
        return "Pais(" + nombre + ", " + continente + ", " + altura + ")";
    }
}
