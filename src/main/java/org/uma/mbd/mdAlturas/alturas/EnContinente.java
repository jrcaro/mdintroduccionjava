package org.uma.mbd.mdAlturas.alturas;

public class EnContinente implements Filtra {
    private String texto;
    public EnContinente(String s){
        texto = s;
    }

    @Override
    public boolean test(Pais pais) {
        return pais.getContinente().contains(texto);
    }
}
