package org.uma.mbd.mdAlturas.alturas;

public class MenoresQue implements Filtra {
    private double altMax;
    public MenoresQue(double max){
        altMax = max;
    }

    @Override
    public boolean test(Pais pais) {
        return pais.getAltura() < altMax;
    }
}
