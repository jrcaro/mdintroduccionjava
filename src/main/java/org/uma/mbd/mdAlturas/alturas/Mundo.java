package org.uma.mbd.mdAlturas.alturas;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Mundo {
    private List<Pais> paises;
    public Mundo(){
        paises = new ArrayList<>();
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void leePaises(String file) throws FileNotFoundException {
        try(Scanner sc = new Scanner(new File(file))) {
            leePaises(sc);
        }
    }

    private void leePaises(Scanner sc){
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            stringToPais(line);
        }
    }

    private void stringToPais(String datos) {
        try(Scanner sc = new Scanner(datos)){
            sc.useDelimiter(",");
            //sc.useLocale(Locale.ENGLISH);
            String nameP = sc.next();
            String cont = sc.next();
            double alt = sc.nextDouble();
            Pais pais = new Pais(nameP, cont, alt);
            paises.add(pais);
        }
    }

    public List<Pais> filtro (Filtra fil){
        List<Pais> sal = new ArrayList<>();
        for(Pais pais : paises){
            if(fil.test(pais)){
                sal.add(pais);
            }
        }
        return sal;
    }

    public Map<String, Integer> numPaisesPorContinente(){
        Map<String, Integer> map = new HashMap<>();
        for (Pais pais: paises) {
            int veces = map.getOrDefault(pais.getContinente(), 0);
            map.put(pais.getContinente(), veces+1);
        }
        return map;
    }

    public Map<String, Set<Pais>> paisesPorContinente(){
        Map<String, Set<Pais>> map = new HashMap<>();
        Comparator<Pais> compA = (p1, p2) -> Double.compare(p1.getAltura(), p2.getAltura());
        Comparator<Pais> compN = Comparator.comparing(Pais::getNombre);
        Comparator<Pais> comp = compA.thenComparing(compN);
        for(Pais pais : paises){
            String continente = pais.getContinente();
            Set<Pais> set = map.computeIfAbsent(continente, c -> new TreeSet<>(comp));
            set.add(pais);
        }
        return map;
    }
}
