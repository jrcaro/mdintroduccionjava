package org.uma.mbd.mdAlturas;

import org.uma.mbd.mdAlturas.alturas.EnContinente;
import org.uma.mbd.mdAlturas.alturas.MenoresQue;
import org.uma.mbd.mdAlturas.alturas.Mundo;
import org.uma.mbd.mdAlturas.alturas.Pais;

import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class MainPrueba {
    public static void main(String args[]) throws FileNotFoundException {
        Mundo paises = new Mundo();
        String path = "src/main/java/org/uma/mbd/mdAlturas/";
        paises.leePaises(path + "alturas.txt");

        Comparator<Pais> oCont = Comparator.comparing(Pais::getContinente);
        Comparator<Pais> oAlt = (s1, s2) -> Double.compare(s1.getAltura(), s2.getAltura());
        Comparator<Pais> oNombre = (t1, t2) -> t1.getNombre().compareTo(t2.getNombre());
        Set<Pais> set1 = new TreeSet<>(oCont.thenComparing(oAlt).thenComparing(oNombre));
        for (Pais pais : paises.filtro(new MenoresQue(1.70))) {
            System.out.println(pais);
        }
        System.out.println("Ordenado");
        set1.addAll(paises.filtro(new MenoresQue(1.70)));
        set1.forEach(System.out::println);

        System.out.println();
        Set<Pais> set2 = new TreeSet<>(oAlt.reversed().thenComparing(oNombre));
        for (Pais pais : paises.filtro(new EnContinente("Europe"))) {
            System.out.println(pais);
        }
        System.out.println("Ordenado");
        set2.addAll(paises.filtro(new EnContinente("Europe")));
        set2.forEach(System.out::println);

        System.out.println();
        paises.filtro(p -> p.getAltura() < 1.65)
                .forEach(System.out::println);

        System.out.println("Ordenado");
        Set<Pais> set3 = new TreeSet<>(paises.filtro(p -> p.getAltura() < 1.65)); //inicializacion a la lista
        set3.forEach(System.out::println);

        paises.numPaisesPorContinente().forEach((k, v) -> System.out.println(k + " -> " + v));

        Map<String, Set<Pais>> map = paises.paisesPorContinente();
        for(String key : map.keySet())
        {
            System.out.println(key);
            Set<Pais> set = map.get(key);
            for(Pais pais : set)
                System.out.println("\t" + pais);
        }
    }
}
