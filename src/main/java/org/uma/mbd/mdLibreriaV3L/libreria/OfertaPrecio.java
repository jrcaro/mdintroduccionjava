package org.uma.mbd.mdLibreriaV3L.libreria;

public class OfertaPrecio implements OfertaFlex {
    private double porDescuento, umbralPrecio;
    public OfertaPrecio(double pD, double uP){
        porDescuento = pD;
        umbralPrecio = uP;
    }

    @Override
    public double getDescuento(Libro l){
        return (l.getPrecioBase() >= umbralPrecio)? porDescuento : 0;
    }

    @Override
    public String toString(){
        return porDescuento + "% (" + umbralPrecio + ")";
    }
}
