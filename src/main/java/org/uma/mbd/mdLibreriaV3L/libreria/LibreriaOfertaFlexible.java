package org.uma.mbd.mdLibreriaV3L.libreria;

public class LibreriaOfertaFlexible extends Libreria {
    private OfertaFlex ofertaFlexible;
    public LibreriaOfertaFlexible(OfertaFlex oF){
        super();
        ofertaFlexible = oF;
    }

    public OfertaFlex getOfertaFlexible() {
        return ofertaFlexible;
    }

    public void setOfertaFlexible(OfertaFlex ofertaFlexible) {
        this.ofertaFlexible = ofertaFlexible;
    }

    @Override
    public void addLibro(String aut, String tit, double pB){
        Libro lib = new Libro(aut, tit, pB);
        double desc = this.getOfertaFlexible().getDescuento(lib);
        if(desc > 0) {
            lib = new LibroEnOferta(aut, tit, pB, desc);
        }
            super.addLibro(lib);
    }

    @Override
    public String toString(){
        return ofertaFlexible.toString() + " [" + super.toString() + "]";
    }
}
