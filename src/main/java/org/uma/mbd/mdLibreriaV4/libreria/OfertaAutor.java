package org.uma.mbd.mdLibreriaV4.libreria;

public class OfertaAutor implements OfertaFlex {
    private String[] autores;
    private double porDescuento;
    public OfertaAutor(double desc, String[] auts){
        autores = auts;
        porDescuento = desc;
    }

    @Override
    public double getDescuento(Libro l){
        return (this.esAutorOferta(l.getAutor()))? porDescuento : 0;
    }

    private boolean esAutorOferta(String aut){
        int i = 0;
        while(i < autores.length && !autores[i].equalsIgnoreCase(aut)){
            i++;
        }
        return i != autores.length;
    }

    @Override
    public String toString(){
        String salida = porDescuento + "% [";
        for(int i = 0; i < autores.length; i++){
            salida += autores[i];
            if(i < autores.length - 1){
                salida += "; ";
            }
        }
        return salida + "]";
    }
}
