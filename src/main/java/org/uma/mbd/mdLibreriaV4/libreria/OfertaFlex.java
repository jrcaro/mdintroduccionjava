package org.uma.mbd.mdLibreriaV4.libreria;

@FunctionalInterface
public interface OfertaFlex {
    double getDescuento(Libro l);
}