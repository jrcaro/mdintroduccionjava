package org.uma.mbd.mdLibreriaV4.libreria;

public class LibroEnOferta extends Libro {
    private double descuento;
    public LibroEnOferta(String aut, String tit, double pBase, double desc){
        super(aut, tit, pBase);
        descuento = desc;
    }

    public double getDescuento() {
        return descuento;
    }

    @Override
    public double getPrecioFinal() {
        double px = super.getPrecioBase() - super.getPrecioBase()*descuento/100;
        return px + px*getIVA()/100; //super.getIVA()?????
    }

    @Override
    public String toString(){
        return "(" + super.getAutor() + "; " + super.getTitulo() +
                "; " + super.getPrecioBase() + "; " + descuento +
                "% ; " + (super.getPrecioBase() - super.getPrecioBase()*descuento/100) + "; " + getIVA() +
                "% ; " + this.getPrecioFinal() + ")";
    }
}
