package org.uma.mbd.mdBancoV1L.banco;

import java.util.ArrayList;
import java.util.List;

public class Banco {
    private static final int PRIMER_NUM_CTA = 1001;
    private String nombre;
    private int snc;
    private List<Cuenta> ctas;
    public Banco(String name){
        nombre = name;
        snc = PRIMER_NUM_CTA;
        ctas = new ArrayList<>();
    }

    public int abrirCuenta(String name, double dinero){
        ctas.add(new Cuenta(name, snc, dinero));
        snc++;
        return snc-1;
    }

    public int abrirCuenta(String name){
        return this.abrirCuenta(name, 0);
    }

    public void cerrarCuenta(int nCuenta){
        int i = posicionCuenta(nCuenta);
        ctas.remove(i);
    }

    private int posicionCuenta(int nCuenta){
        int i = 0;
        while((i < ctas.size()) && !(ctas.get(i).getNumCuenta() == nCuenta)){
            i++;
        }

        if(i == ctas.size()){
            throw  new RuntimeException("No existe la cuenta dada");
        }
        return i;
    }

    public void ingreso(int nCuenta, double dinero){
        int i = posicionCuenta(nCuenta);
        ctas.get(i).ingreso(dinero);
    }

    public  void debito(int nCuenta, double dinero) {
        int i = posicionCuenta(nCuenta);
        if(ctas.get(i).getSaldo() < dinero){
            ctas.get(i).debito(ctas.get(i).getSaldo());
        } else {
            ctas.get(i).debito(dinero);
        }

    }

    public double saldo(int nCuenta){
        int i = posicionCuenta(nCuenta);
        return ctas.get(i).getSaldo();
    }

    public void transferencia(int nCuenta1, int nCuenta2, double dinero){ //return double??
        int i = posicionCuenta(nCuenta1);
        int j = posicionCuenta(nCuenta2);
        ctas.get(i).ingreso(dinero);
        ctas.get(i).debito(dinero);
    }

    @Override
    public String toString(){
        return ctas.toString();
    }
}
