package org.uma.mbd.mdJarras.jarras;

public class Mesa {
    private Jarra jarraA, jarraB;
    public Mesa(int a, int b){jarraA = new Jarra(a); jarraB = new Jarra(b);}
    public void llenaA(){jarraA.llenar();}
    public void llenaB(){jarraB.llenar();}
    public void vaciaA(){jarraA.vaciar();}
    public void vaciaB(){jarraB.vaciar();}
    public void vuelcaASobreB(){jarraB.llenarDesde(jarraA);}
    public void vuelcaBSobreA(){jarraA.llenarDesde(jarraB);}
    public int getCapacidadA(){return jarraA.getCapacidad();}
    public int getContenidoA(){return jarraA.getContenido();}
    public int getCapacidadB(){return jarraB.getCapacidad();}
    public int getContenidoB(){return jarraB.getContenido();}
    public int getContenido(){return jarraA.getContenido() + jarraB.getContenido();}
    public String toString(){
        return "Mesa(" + jarraA + ", " + jarraB + ")";
    }
}
