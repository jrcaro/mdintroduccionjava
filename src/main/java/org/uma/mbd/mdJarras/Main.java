package org.uma.mbd.mdJarras;

import org.uma.mbd.mdJarras.jarras.Jarra;
import org.uma.mbd.mdJarras.jarras.Mesa;

public class Main {
    public static void main(String [] args){
        /*Jarra j1 = new Jarra(5);
        Jarra j2 = new Jarra(7);

        j1.llenar(); //5,5 7,0
        j2.llenarDesde(j1); //5,0 7,5
        j1.llenar(); //5,5 7,5
        j2.llenarDesde(j1); //5,3 7,7
        j2.vaciar(); //5,3 7,0
        j2.llenarDesde(j1); //5,0 7,3
        j1.llenar();// 5,5 7,3
        j2.llenarDesde(j1); // 5,1 7,7
        System.out.println(j1);
        System.out.println(j2);*/

        Mesa m = new Mesa(5,7);

        m.llenaA();
        m.vuelcaASobreB();
        m.llenaA();
        m.vuelcaASobreB();
        m.vaciaB();
        m.vuelcaASobreB();
        m.llenaA();
        m.vuelcaASobreB();
        System.out.println(m);
    }
}
