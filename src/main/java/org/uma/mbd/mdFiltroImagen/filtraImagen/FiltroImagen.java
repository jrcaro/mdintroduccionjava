package org.uma.mbd.mdFiltroImagen.filtraImagen;

import java.awt.image.BufferedImage;
public interface FiltroImagen {
    void filtra(BufferedImage image);
}
