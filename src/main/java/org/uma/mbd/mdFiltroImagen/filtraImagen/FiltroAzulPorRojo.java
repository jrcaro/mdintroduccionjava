package org.uma.mbd.mdFiltroImagen.filtraImagen;

import java.awt.*;
import java.awt.image.BufferedImage;

public class FiltroAzulPorRojo implements FiltroImagen {
    @Override
    public void filtra(BufferedImage image){
        int fWidth = image.getWidth();
        int fHeight = image.getHeight();

        for(int x = 0; x < fWidth; x++) {
            for (int y = 0; y < fHeight; y++) {
                int azul = new Color(image.getRGB(x, y)).getBlue();
                int rojo = new Color(image.getRGB(x, y)).getRed();
                image.setRGB(x, y, new Color(azul, 0, rojo).getRGB());
            }
        }
    }
}
