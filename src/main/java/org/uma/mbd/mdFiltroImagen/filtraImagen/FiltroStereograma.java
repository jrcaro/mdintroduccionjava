package org.uma.mbd.mdFiltroImagen.filtraImagen;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class FiltroStereograma implements FiltroImagen {
    private static final int TAM_TRAMA = 80;
    private static final int NUM_CAPAS = 16;
    private static final Random aleatorio = new Random();
    private int tamTrama, numCapas;
    public FiltroStereograma(int tamTrama, int numCapas){
        this.numCapas = numCapas;
        this.tamTrama = tamTrama;
    }

    public FiltroStereograma(){
        this.numCapas = TAM_TRAMA;
        this.tamTrama = NUM_CAPAS;
    }

    @Override
    public void filtra(BufferedImage image){
        int fWidth = image.getWidth();
        int fHeight = image.getHeight();

        for (int x = 0; x < tamTrama; x++) {
            for (int y = 0; y < fHeight; y++) {
                Color color = new Color( aleatorio.nextInt(256),
                        aleatorio.nextInt(256),
                        aleatorio.nextInt(256));
                image.setRGB(x, y, color.getRGB());
            }
        }

        for (int x = tamTrama; x < fWidth; x++) {
            for (int y = 0; y < fHeight; y++) {
                int azul = new Color(image.getRGB(x, y)).getBlue();
                int capa = (255 - azul) / (255/numCapas);
                int nx = x - tamTrama + capa;
                image.setRGB(x, y, image.getRGB(nx, y ));
            }
        }
    }
}
