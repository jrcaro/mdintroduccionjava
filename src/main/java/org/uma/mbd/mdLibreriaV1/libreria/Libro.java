package org.uma.mbd.mdLibreriaV1.libreria;

public class Libro {
    private String autor, titulo; //variables de instancia
    private double precioBase;
    private static double IVA = 10; //static -> variable de clase.
                                    // Se evalua en la carga de la maquina
    public Libro(String aut, String tit, double pb){
        autor = aut;
        titulo = tit;
        precioBase = pb;
    }

    public String getAutor() {
        return autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public double getPrecioFinal(){
        return precioBase + precioBase*IVA/100;
    }

    public static double getIVA() {
        return IVA;
    }

    public static void setIVA(double IVA) {
        Libro.IVA = IVA; //variable de clase, no existe objeto (no this, clase)
    }

    @Override
    public String toString() {
        return "(" + autor + ";" + titulo + ";" +
                precioBase + ";" + IVA + ";" +
                getPrecioFinal() + ")";
    }
}
