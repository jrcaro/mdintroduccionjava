package org.uma.mbd.mdLibreriaV1.libreria;

import java.util.Arrays;
import java.util.StringJoiner;

public class Libreria {
    private Libro [] libros;
    private int numLibros;
    private static final int TAM_DEFECTO = 16; //constante
    public Libreria(int tam){
        libros = new Libro[tam];
        numLibros = 0;
    }
    public Libreria(){
        this(TAM_DEFECTO); //libros = new Libro[TAM_DEFECTO];
    }

    public void addLibro(String aut, String tit, double p){
        Libro libro = new Libro(aut,tit,p);
        addLibro(libro);
    }

    private void addLibro(Libro l){
        int i = posicionLibro(l.getAutor(), l.getTitulo());
        if(i < 0){
            aseguraQueCabe();
            libros[numLibros] = l;
            numLibros++;
        } else {
            libros[i] = l;
        }
    }

    private void aseguraQueCabe(){
        if(numLibros == libros.length){
            libros = Arrays.copyOf(libros, numLibros*2);
        }
    }

    private int posicionLibro(String aut, String tit){
        int i = 0;
        while(i < numLibros &&
                !(aut.equalsIgnoreCase(libros[i].getAutor()) &&
                tit.equalsIgnoreCase(libros[i].getTitulo()))){
            i++;
        }
        return (i == numLibros)? -1 : i; //operador ternario
    }

    public void remLibro(String aut, String tit){
        int i = posicionLibro(aut, tit);
        if(i >= 0){
            for(int j = i+1; j < numLibros; j++){
                libros[j-1] = libros[j];
            }
            numLibros--;
        }
    }

    public double getPrecioBase(String aut, String tit){
        int i = posicionLibro(aut, tit);
        return (i >= 0)? libros[i].getPrecioBase() : 0;
    }

    public double getPrecioFinal(String aut, String tit){
        int i = posicionLibro(aut, tit);
        return (i >= 0)? libros[i].getPrecioFinal() : 0;
    }

    public String toString(){ //mejorado toString
        StringJoiner sj = new StringJoiner(",","[", "]");
        for(int i = 0; i < numLibros; i++){
            sj.add(libros[i].toString());
        }
        return sj.toString();
        /*StringBuilder sb = new StringBuilder("[");
        for(int i = 0; i < numLibros - 1; i++){
            sb.append(libros[i]);
            sb.append(", ");
        }
        if(libros.length > 0){
            sb.append(libros[numLibros-1]);
        }
        sb.append("]");
        return sb.toString();*/
    }
}
