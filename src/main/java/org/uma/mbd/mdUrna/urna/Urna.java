package org.uma.mbd.mdUrna.urna;

import java.util.NoSuchElementException;
import java.util.Random;

public class Urna {
    public static enum ColorBola {Blanca, Negra};
    private static Random alea = new Random();
    private int blancas, negras;
    public Urna(int nb, int nn){
        if((nb >= 0) && (nn >= 0) && (nb + nn > 0)){
            blancas = nb;
            negras = nn;
        } else {
            throw new IllegalArgumentException("Bolas invalidas");
        }
    }

    public int totalBolas(){return blancas+negras;}

    public void ponerBlanca(){
        blancas++;
    }

    public void ponerNegra(){
        negras++;
    }

    public ColorBola extraerBola(){
        int t = totalBolas();
        ColorBola salida;
        int va = alea.nextInt(t);

        if(t == 0){
            throw new NoSuchElementException("No hay bolas");
        }

        if(va < blancas){
            //blanca
            salida = ColorBola.Blanca;
            blancas--;
        } else {
            //negra
            salida = ColorBola.Negra;
            negras--;
        }
        return salida;
    }

    @Override
    public String toString() {
        return "Urna{" + blancas + ", " + negras + '}';
    }
}
