package org.uma.mbd.mdUrna;

import org.uma.mbd.mdUrna.urna.Urna;

public class Main {
    public static void main(String[] args) {
        //pasar por argumento
        Urna u = new Urna(Integer.parseInt(args[0]), Integer.parseInt(args[1]));

        while(u.totalBolas() > 1){
            Urna.ColorBola b1 = u.extraerBola();
            Urna.ColorBola b2 = u.extraerBola();
            if(b1 == b2){
                u.ponerBlanca();
            } else {
                u.ponerNegra();
            }
        }
        System.out.println(u.extraerBola());
    }
}
